import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Score {
  final int id;
  final int leftScore;
  final int rightScore;
  final String screenShotInBase64;

  Score({this.id, this.leftScore, this.rightScore, this.screenShotInBase64});

  factory Score.fromJson(Map<String, dynamic> json) {
    return Score(
      id: json['id'],
      leftScore: json['left_score'],
      rightScore: json['right_score'],
      screenShotInBase64: json['goal_sshot'] as String,
    );
  }
}

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Score _score = new Score(id: 0, leftScore: 5, rightScore: 5, screenShotInBase64: "");
  Image goalScreenshot = Image.asset('assets/images/init_logo.png');
  bool _isInit = true;

  void _fetchScore() {
    http.get('http://192.168.31.103:8000/read').then(_processResponse);
  }
  
  void _processResponse(http.Response res) {
    final resBody = json.decode(res.body);

    if (res.statusCode == 200) {
      setState(() {
        _score = Score.fromJson(resBody[0]);
        goalScreenshot = Image.memory(base64Decode(_score.screenShotInBase64.substring(22)));
        _isInit = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_isInit) {
      return MaterialApp(
        title: 'Fetch Data Example',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text('Fetch Data Example'),
          ),
          body: Center(
            child: Column(
              children: <Widget> [
                goalScreenshot,
                Align(
                  alignment: Alignment.bottomCenter,
                  child: RaisedButton(
                    onPressed: _fetchScore,
                    child: const Text('Fetch score!', style: TextStyle(fontSize: 20)),
                    color: Colors.amberAccent,
                    textColor: Colors.white70,
                    elevation: 5,
                  ),
                ),
              ]
            ),
          ),
        ),
      );
    } else {
      return MaterialApp(
        title: 'Fetch Data Example',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text('Fetch Data Example'),
          ),
          body: Center(
              child:
              Column(
                  children: <Widget>[
                    Text(
                      'Left player score:' + _score.leftScore.toString(),
                    ),
                    Text(
                      'Right player score:' + _score.rightScore.toString(),
                    ),
                    Text(
                      'Last goal replay:',
                    ),
                    goalScreenshot,
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: RaisedButton(
                        onPressed: _fetchScore,
                        child: const Text('Fetch score!', style: TextStyle(fontSize: 20)),
                        color: Colors.amberAccent,
                        textColor: Colors.white70,
                        elevation: 5,
                      ),
                    ),
                  ]
              )
          ),
        ),
      );
    }
  }
}
